// include Fake lib
#r @"packages/build/FAKE/tools/FakeLib.dll"
open Fake
open Fake.AssemblyInfoFile

// Properties
let buildDir = "./build/transdok.deployment"
let octoDir = "./build/octo"

let version =  getBuildParamOrDefault "version" "0.0.0.1"


// Targets
Target "Clean" (fun _ ->
    CleanDirs [buildDir]
)

Target "Restore" (fun _ ->
    RestorePackages()
)

Target "Build" (fun _ ->
    let p = ["Platform", "AnyCPU"]
    
    !! "src/transdok.deployment/transdok.deployment.csproj"
    |> MSBuildReleaseExt buildDir p "Build"
    |> ignore
)

Target "AssemblyInfo" (fun _ ->
    CreateCSharpAssemblyInfo "./src/CommonAssemblyInfo.cs" 
        [
            Attribute.Version version
            Attribute.FileVersion version            
            Attribute.ComVisible false
            Attribute.Product "transdok.deploy"
            Attribute.Company "motile users Software GmbH"
        ]
)

Target "Octo" (fun _ ->    
    CreateDir(octoDir)
    NuGet(fun p ->
            {p with
                Version = version
                OutputPath = octoDir
                WorkingDir = buildDir + @"\_PublishedWebsites\transdok.deployment"
                Files = [ (@"*\/*.*", None, None) ] 
        })  "./src/transdok.deployment/transdok.deployment.nuspec"
)
    

Target "All" DoNothing



// Dependencies
"Clean"    
    ==> "Build" 

"AssemblyInfo"
    ==> "Build"

"Build"
    ==> "Octo"


// start build
RunTargetOrDefault "All"