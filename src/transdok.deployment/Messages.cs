﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace transdok.deployment
{
    public class Messages
    {
        private class Msg
        {
            public Msg(string msg)
            {
                this.Date = DateTime.Now;
                this.Message = msg;
            }
            public DateTime Date { get; }
            public string Message { get; }
        }

        private ConcurrentQueue<Msg> _msg = new ConcurrentQueue<Msg>();

        private static Messages _instance;
        public static Messages Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Messages();
                }
                return _instance;
            }
        }

        private Messages()
        {

        }

        public string GetMessages()
        {
            if (_msg == null)
                return string.Empty;

            return string.Join(Environment.NewLine, _msg.Select(x => $"{x.Date.ToLongTimeString()}: {x.Message}"));
        }
        public void AddMsg(string msg, string fallback = ".")
        {
            if (string.IsNullOrEmpty(msg))
                _msg.Enqueue(new Msg(fallback));
            else
                _msg.Enqueue(new Msg(msg));


            Clean();
        }

        private void Clean()
        {
            var t = DateTime.Now.AddSeconds(-60);
            var m = _msg.Where(x => x.Date < t).LastOrDefault();

            if (m != null)
            {
                bool ok = true;
                while (ok)
                {
                    ok = _msg.TryDequeue(out Msg mm);
                    ok = ok && !object.ReferenceEquals(mm, m) && _msg.Count > 10;
                }
            }
        }

    }
}