﻿using Serilog;
using System.IO;
using System.Threading.Tasks;

namespace transdok.deployment
{
    public class GitDeployer
    {        
        private readonly string _path;
        private readonly string _pathRepository;
        private AsyncLock _lock = new AsyncLock();
        private readonly string[] _branches;
        private readonly string _repository;
     
        internal GitDeployer(string repository, string path, string[] branches)
        {
            _repository = repository;
            _path = path;
            _pathRepository = Path.Combine(path, "td13_deployment");
            _branches = branches;
        }
     
        public bool IsBusy()
        {
            return _lock.IsLocked();
        }

        public async Task Kick()
        {
            if (_lock.IsLocked())
            {
                return;
            }

            using (await _lock.LockAsync())
            {
                Log.Information("started");

                if (!Directory.Exists(_pathRepository))
                {
                    Directory.CreateDirectory(_pathRepository);
                }

                if (!Directory.Exists(Path.Combine(_pathRepository, ".git")))
                {                
                    await GitAsync($"clone {_repository} {_pathRepository}");                 
                }
                
                await GitAsync($"fetch --all");
                
                await GitAsync($@" reset --hard origin/master");                
                await GitAsync($@" checkout --force master");
                await GitAsync($@" clean -f -d");

                foreach (var branch in _branches)
                {
                    await GitAsync($@"branch -D {branch}");
                    await GitAsync($@"checkout --force {branch}");

                    await GitLfsAsync($@"fetch");
                    await GitLfsAsync($@"checkout");

                    var branchdir = Path.Combine(_path, branch);
                    PathHelper.SyncPath(_pathRepository, branchdir);

                }

                await GitAsync($@" checkout --force master");

                await GitLfsAsync("prune");
            }

            Log.Information("done");
        }

        private Task GitAsync(string cmd)
        {
            return Task.Run(() => Git(cmd));
        }

        private Task GitLfsAsync(string cmd)
        {
            return Task.Run(() => GitLfs(cmd));
        }

        private void Git(string cmd)
        {
            Log.Information(cmd);

            var pinfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = "git",
                Arguments = cmd,
                WorkingDirectory = _pathRepository,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var p = System.Diagnostics.Process.Start(pinfo);

            string output = p.StandardOutput.ReadToEnd();
            if (!string.IsNullOrEmpty(output))
                Log.Information(output);

            output = p.StandardError.ReadToEnd();
            if (!string.IsNullOrEmpty(output))
                Log.Information(output);
        }

        private void GitLfs(string cmd)
        {
            Log.Information(cmd);

            var pinfo = new System.Diagnostics.ProcessStartInfo()
            {
                FileName = @"C:\Program Files\Git LFS\git-lfs.exe",
                Arguments = cmd,
                WorkingDirectory = _pathRepository,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };


            var p = System.Diagnostics.Process.Start(pinfo);

            string output = p.StandardOutput.ReadToEnd();
            if (!string.IsNullOrEmpty(output))
                Log.Information(output);

            output = p.StandardError.ReadToEnd();
            if (!string.IsNullOrEmpty(output))
                Log.Information(output);
        }        
    }
}