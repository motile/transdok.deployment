﻿using Serilog;
using System;
using System.Threading.Tasks;

namespace transdok.deployment
{
    public class HangfireGitDeployKicker
    {
        private static DateTime _lastkick;
        private static AsyncLock _lock = new AsyncLock();

        public async Task Kick(DateTime date)
        {
            using (await _lock.LockAsync())
            {
                if (date < _lastkick)
                {
                    Log.Information("skip Kick");

                    return;
                }

                _lastkick = DateTime.Now;
            }

            await GitDeploySingleton.Instance.Kick();
        }
    }
}