﻿using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.Owin;
using Owin;
using Serilog;
using System;
using System.Reactive.Linq;
using System.Web.Http;

[assembly: OwinStartup(typeof(transdok.deployment.Startup))]

namespace transdok.deployment
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Observers(events => events
                    .Do(evt =>
                    {                        
                        Messages.Instance.AddMsg($"{evt.MessageTemplate.Text}");
                    })
                    .Subscribe())
                    .CreateLogger();

            Log.Logger.Information("startup");

            Hangfire.GlobalConfiguration.Configuration.UseMemoryStorage();
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            config.InitializeReceiveBitbucketWebHooks();

            app.UseWebApi(config);
        }
    }
}
