﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;

namespace transdok.deployment
{
    public class GitDeploySingleton
    {

        private static GitDeployer _gitdeployer;
        private static object _lock = new object();

        private GitDeploySingleton()
        {

        }

        public static GitDeployer Instance
        {
            get
            {
                if (_gitdeployer == null)
                {
                    lock (_lock)
                    {
                        if (_gitdeployer == null)
                        {

                            var repository = ConfigurationManager.AppSettings["Repository"];

                            var path = ConfigurationManager.AppSettings["GitDeployPath"];

                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }

                            var branches = ConfigurationManager
                                .AppSettings["GitDeployBranches"]
                                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                .Select(x => x.Trim())
                                .Where(x => !string.IsNullOrEmpty(x))
                                .ToArray();

                            _gitdeployer = new GitDeployer(repository, path, branches);
                        }
                    }
                }

                return _gitdeployer;
            }
        }
    }
}