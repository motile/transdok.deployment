﻿using Hangfire;
using Serilog;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace transdok.deployment.Controller
{
    [RoutePrefix("api/deployment")]
    public class DeploymentController : ApiController
    {
        [Route("kick")]
        [HttpGet]
        public async Task<IHttpActionResult> Kick()
        {
            var g = GitDeploySingleton.Instance;

            if (!g.IsBusy())
            {
                Log.Information("Kick");

                var d = DateTime.Now;
                var id = BackgroundJob.Enqueue<HangfireGitDeployKicker>(x => x.Kick(d));
                await Task.Delay(100);
            }

            return RedirectToRoute("default", null);
        }

        [Route("", Name = "default")]
        [HttpGet]
        public IHttpActionResult GetMessages()
        {
            var info = Messages.Instance.GetMessages();

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(info);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

            return ResponseMessage(response);
        }
    }
}