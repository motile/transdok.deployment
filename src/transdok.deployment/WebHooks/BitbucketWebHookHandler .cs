﻿using Hangfire;
using Microsoft.AspNet.WebHooks;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace transdok.deployment.WebHooks
{
    public class BitbucketWebHookHandler : WebHookHandler
    {
        public BitbucketWebHookHandler()
        {
            this.Receiver = BitbucketWebHookReceiver.ReceiverName;
        }

        public override Task ExecuteAsync(string generator, WebHookHandlerContext context)
        {
            Serilog.Log.Information("Exec");
            // For more information about BitBucket WebHook payloads, please see 
            // 'https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html#EventPayloads-Push'
            //JObject entry = context.GetDataOrDefault<JObject>();

            // Extract the action -- for Bitbucket we have only one.
            try
            {
                string action = context.Actions.First();
                Serilog.Log.Information(action);
            }
            catch (Exception e)
            {
                Serilog.Log.Error(e, "");
            }

            var d = DateTime.Now;
            var id = BackgroundJob.Enqueue<HangfireGitDeployKicker>(x => x.Kick(d));

            return Task.FromResult(true);
        }

    }

}