﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;

namespace transdok.deployment
{
    public class PathHelper
    {
        //verzeichnisse, die mit einem . anfangen, sollen ignoriert werden (z.b. .git)
        private static Regex _skipDirecoryPattern = new Regex(@"\.\w*$");

        public static void SyncPath(string fromPath, string toPath)
        {
            if (!Directory.Exists(toPath))
            {
                Directory.CreateDirectory(toPath);
            }

            //Verzeichnisse OHNE abschließendem  @"\"
            toPath = Path.GetDirectoryName(toPath + @"\");
            fromPath = Path.GetDirectoryName(fromPath + @"\");

            // zuerst das Zielverzeichnis säubern
            CleanTargetDir(fromPath, toPath);

            // noch nicht vorhandene Dateien kopieren
            CopySource(fromPath, toPath);
        }

        private static void RemoveEmptyFolders(string path)
        {
            foreach (var directory in Directory.GetDirectories(path))
            {
                RemoveEmptyFolders(directory);

                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }

        }

        private static void CopySource(string fromPath, string toPath)
        {
            var directories = Directory.GetDirectories(fromPath).Where(x => !_skipDirecoryPattern.IsMatch(x)).ToList();
            foreach (var directory in directories)
            {
                foreach (var fileInSource in Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories))
                {
                    var target = Path.Combine(toPath, fileInSource.Substring(fromPath.Length + 1));

                    if (!File.Exists(target))
                    {
                        //groß/kleinschreibung korrigieren
                        target = Path.Combine(Path.GetDirectoryName(target), Path.GetFileName(fileInSource));

                        Serilog.Log.Information($"copy {Path.GetFileName(fileInSource)}");

                        if (!Directory.Exists(Path.GetDirectoryName(target)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(target));
                        }

                        File.Copy(fileInSource, target);
                    }
                }
            }
        }

        private static void CleanTargetDir(string fromPath, string toPath)
        {
            // es werden derzeit nur Unterverzeichnisse synchronisizert. 
            // ggf. das Hauptverzeichnis auch berücksichtigen, dann aber SearchOption.TopDirectoryOnly!
            var directories = Directory.GetDirectories(toPath).Where(x => !_skipDirecoryPattern.IsMatch(x)).ToList();

            foreach (var directory in directories)
            {
                foreach (var fileInTarget in Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories))
                {
                    bool skip = false;

                    var source = Path.Combine(fromPath, fileInTarget.Substring(toPath.Length + 1));

                    if (File.Exists(source))
                    {
                        var targetFileInfo = new FileInfo(fileInTarget);

                        //FileInfo mit korrekter groß/kleinschreibung
                        var sourceFileInfo = new DirectoryInfo(Path.GetDirectoryName(source)).GetFiles(Path.GetFileName(source)).First();

                        skip = (targetFileInfo.Length == sourceFileInfo.Length);

                        skip = skip && string.Equals(targetFileInfo.Name, sourceFileInfo.Name, StringComparison.CurrentCulture);  //groß/kleinschreibung

                        skip = skip && CompareHash(fileInTarget, source);
                    }

                    if (!skip)
                    {
                        Serilog.Log.Information($"remove {Path.GetFileName(fileInTarget)}");
                        File.Delete(fileInTarget);
                    }
                }
            }

            RemoveEmptyFolders(toPath);
        }

        private static bool CompareHash(string f1, string f2)
        {
            using (var md5 = MD5.Create())
            {
                string h1;
                string h2;
                using (var stream = File.OpenRead(f1))
                {
                    h1 = Convert.ToBase64String(md5.ComputeHash(stream));
                }

                using (var stream = File.OpenRead(f2))
                {
                    h2 = Convert.ToBase64String(md5.ComputeHash(stream));
                }


                return h1 == h2;
            }
        }
    }
}