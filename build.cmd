@echo off

if '%1'=='/?' goto help
if '%1'=='-help' goto help
if '%1'=='--help' goto help
if '%1'=='-h' goto help

if '%1'=='Cleaner' goto fake
if '%1'=='cleaner' goto fake

.paket\paket.exe restore
if errorlevel 1 (
    exit /b %errorlevel%
)

:fake
packages\build\FAKE\tools\FAKE.exe build.fsx %*

goto exit


:help
echo build octo version=0.0.2

:exit